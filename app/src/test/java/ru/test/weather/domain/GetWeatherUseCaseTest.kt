package ru.test.weather.domain

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import ru.test.weather.common.constants.BusinessErrorType
import ru.test.weather.common.dto.Result
import ru.test.weather.common.map
import ru.test.weather.datasource.remote.WeatherDataSource
import ru.test.weather.domain.usecases.GetWeatherUseCase
import ru.test.weather.mocks.Mocks
import ru.test.weather.repository.WeatherRepositoryImp

class GetWeatherUseCaseTest {

    @Test
    fun `useCase should return right result with correct response`() = runBlocking {
        val expected = Mocks.weatherEntity.copy()
        val weatherRepository = mock<WeatherRepository> {
            onBlocking {
                getWeather()
            } doReturn Mocks.weatherEntity
        }

        val weatherUseCase = GetWeatherUseCase(weatherRepository)
        weatherUseCase.invoke(this, Unit) {
            Assert.assertTrue(it.isRight)
            it.map { weatherEntity ->
                Assert.assertEquals(weatherEntity, expected)
            }
        }
    }

    @Test
    fun `useCase should return left result on error in request` () = runBlocking {
        val weatherDataSource = mock<WeatherDataSource> {
            onBlocking {
                getWeather()
            } doReturn Result.error(BusinessErrorType.NETWORK_ERROR)
        }
        val weatherRepository: WeatherRepository = WeatherRepositoryImp(weatherDataSource)

        val weatherUseCase = GetWeatherUseCase(weatherRepository)
        weatherUseCase.invoke(this, Unit) {
            Assert.assertTrue(it.isLeft)
        }
    }
}