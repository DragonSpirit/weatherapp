package ru.test.weather.repository

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import ru.test.weather.common.constants.BusinessErrorType
import ru.test.weather.common.dto.Result
import ru.test.weather.datasource.remote.WeatherDataSource
import ru.test.weather.domain.WeatherRepository
import ru.test.weather.mocks.Mocks
import java.lang.Exception

class WeatherRepositoryTest {
    @Test
    fun `should return correct WeatherResponse`() = runBlocking {
        val expected = Mocks.weatherEntity.copy()
        val weatherDataSource = mock<WeatherDataSource> {
            onBlocking {
                getWeather()
            } doReturn Result.success(Mocks.weatherResponse)
        }
        val weatherRepository: WeatherRepository = WeatherRepositoryImp(weatherDataSource)
        val result = weatherRepository.getWeather()

        Assert.assertEquals(expected, result)
    }

    @Test(expected = Exception::class)
    fun `should throw an exception with error in WeatherResponse` () = runBlocking {
        val weatherDataSource = mock<WeatherDataSource> {
            onBlocking {
                getWeather()
            } doReturn Result.error(BusinessErrorType.NETWORK_ERROR)
        }
        val weatherRepository: WeatherRepository = WeatherRepositoryImp(weatherDataSource)
        weatherRepository.getWeather()
        return@runBlocking
    }
}