package ru.test.weather.mapper

import org.junit.Assert
import org.junit.Test
import ru.test.weather.domain.model.WeatherEntity
import ru.test.weather.mocks.Mocks
import ru.test.weather.repository.mapper.WeatherAPIMapper

class MapperTest {
    @Test
    fun `Should map empty response to correct weather entity`() {
        val result: WeatherEntity = WeatherAPIMapper.mapFrom(null)
        Assert.assertEquals(result, Mocks.failedWeatherEntity)
    }

    @Test
    fun `Should map correct weather response to correct weather entity`() {
        val result: WeatherEntity = WeatherAPIMapper.mapFrom(Mocks.weatherResponse)
        val expected = Mocks.weatherEntity
        Assert.assertEquals(result, expected)
    }
}