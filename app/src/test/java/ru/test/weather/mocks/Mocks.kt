package ru.test.weather.mocks

import ru.test.weather.datasource.remote.models.Main
import ru.test.weather.datasource.remote.models.Weather
import ru.test.weather.datasource.remote.models.WeatherResponse
import ru.test.weather.datasource.remote.models.Wind
import ru.test.weather.domain.model.WeatherEntity

object Mocks {
    val weatherResponse = WeatherResponse(
        weather = listOf(
            Weather(
                id = 1,
                main = "test",
                description = "cloud",
                icon = "any"
            )
        ),
        main = Main(
            temp = 10.1,
            temp_max = 10.5,
            temp_min = 10.0,
            pressure = 5,
            humidity = 1
        ),
        visibility = 1,
        wind = Wind(
            speed = 5,
            deg = 10
        ),
        id = 1,
        dt = 1,
        name = "Test"
    )

    val weatherEntity =  WeatherEntity(
        id = 1,
        description = "cloud",
        windSpeed = 5,
        name = "Test",
        temp = 10.1
    )

    val failedWeatherEntity = WeatherEntity(
        id = -1,
        description = "",
        windSpeed = -1,
        name = "Unknown",
        temp = (-1).toDouble()
    )
}