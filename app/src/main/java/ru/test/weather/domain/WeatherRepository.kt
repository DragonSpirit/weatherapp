package ru.test.weather.domain

import ru.test.weather.domain.model.WeatherEntity

interface WeatherRepository {
    suspend fun getWeather(): WeatherEntity
}