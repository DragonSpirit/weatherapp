package ru.test.weather.domain.model

data class WeatherEntity (
    val description: String,
    val temp : Double,
    val windSpeed : Int,
    val id : Int,
    val name : String
)