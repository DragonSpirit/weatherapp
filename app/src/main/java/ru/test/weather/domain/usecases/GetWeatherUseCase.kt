package ru.test.weather.domain.usecases

import ru.test.weather.common.Either
import ru.test.weather.domain.BaseUseCase
import ru.test.weather.domain.Failure
import ru.test.weather.domain.WeatherRepository
import ru.test.weather.domain.model.WeatherEntity

class GetWeatherUseCase (private val weatherRepository: WeatherRepository): BaseUseCase<WeatherEntity, Unit>() {

    override suspend fun run(params: Unit): Either<Failure, WeatherEntity> {
        return try {
            val result = weatherRepository.getWeather()
            Either.Right(result)
        } catch (ex: java.lang.Exception) {
            Either.Left(GetWeatherFailure(ex))
        }
    }


    data class GetWeatherFailure(val error: Exception) : Failure.FeatureFailure(error)

}