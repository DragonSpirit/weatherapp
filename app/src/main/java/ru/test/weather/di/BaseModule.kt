package ru.test.weather.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import ru.test.weather.datasource.remote.WeatherDataSource
import ru.test.weather.datasource.remote.WeatherDataSourceImp
import ru.test.weather.datasource.remote.services.WeatherAPIService
import ru.test.weather.domain.WeatherRepository
import ru.test.weather.domain.usecases.GetWeatherUseCase
import ru.test.weather.repository.WeatherRepositoryImp
import ru.test.weather.viewmodels.WeatherViewModel

object BaseModule {
    val mainModule = module {
        single { provideWeatherApiService(get()) }
        single { WeatherDataSourceImp(weatherAPIService = get()) as WeatherDataSource }
        single { WeatherRepositoryImp(weatherDataSource = get()) as WeatherRepository }
        single { GetWeatherUseCase(weatherRepository = get()) }
        viewModel { WeatherViewModel(getWeatherUseCase = get()) }
    }

    private fun provideWeatherApiService(retrofit: Retrofit): WeatherAPIService {
        return retrofit.create(WeatherAPIService::class.java)
    }
}