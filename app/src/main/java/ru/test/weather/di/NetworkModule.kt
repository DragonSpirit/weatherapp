package ru.test.weather.di

import com.google.gson.GsonBuilder
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkModule {
    private const val URL_BASE = "http://api.openweathermap.org/data/2.5/"

    val retrofitModule = module {
        single { provideRetrofitInstance() }
    }

    private fun provideRetrofitInstance(): Retrofit = Retrofit.Builder()
        .baseUrl(URL_BASE)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()
}