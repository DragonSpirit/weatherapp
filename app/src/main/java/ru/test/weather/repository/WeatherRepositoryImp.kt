package ru.test.weather.repository

import ru.test.weather.common.enums.ResultType
import ru.test.weather.datasource.remote.WeatherDataSource
import ru.test.weather.domain.WeatherRepository
import ru.test.weather.domain.model.WeatherEntity
import ru.test.weather.repository.mapper.WeatherAPIMapper
import java.lang.Exception

class WeatherRepositoryImp constructor(
    private val weatherDataSource: WeatherDataSource
): WeatherRepository {

    private lateinit var weatherEntity: WeatherEntity

    override suspend fun getWeather(): WeatherEntity {
        weatherDataSource.getWeather()?.let { weatherResponse ->
            if (weatherResponse.resultType == ResultType.SUCCESS) {
                WeatherAPIMapper.mapFrom(weatherResponse.data).let { this.weatherEntity = it }
            } else {
                throw Exception(weatherResponse.error.toString())
            }
        }
        return weatherEntity
    }
}