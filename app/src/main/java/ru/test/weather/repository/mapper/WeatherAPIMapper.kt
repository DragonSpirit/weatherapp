package ru.test.weather.repository.mapper

import ru.test.weather.common.BaseMapper
import ru.test.weather.datasource.remote.models.WeatherResponse
import ru.test.weather.domain.model.WeatherEntity

object WeatherAPIMapper: BaseMapper<WeatherResponse, WeatherEntity> {
    override fun mapFrom(type: WeatherResponse?): WeatherEntity {
        return WeatherEntity(
            description = type?.weather?.get(0)?.description ?: "",
            windSpeed = type?.wind?.speed ?: -1,
            temp = type?.main?.temp ?: -1.0,
            id = type?.id ?: -1,
            name = type?.name ?: "Unknown"
        )
    }
}