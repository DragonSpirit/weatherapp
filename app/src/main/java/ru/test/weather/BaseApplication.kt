package ru.test.weather

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import ru.test.weather.di.BaseModule
import ru.test.weather.di.NetworkModule

class BaseApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@BaseApplication)
            modules(
                listOf(
                    BaseModule.mainModule,
                    NetworkModule.retrofitModule
                )
            )
        }
    }
}