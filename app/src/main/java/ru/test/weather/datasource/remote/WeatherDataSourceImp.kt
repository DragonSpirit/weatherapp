package ru.test.weather.datasource.remote

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.test.weather.common.constants.BusinessErrorType
import ru.test.weather.common.dto.Result
import ru.test.weather.datasource.remote.models.WeatherResponse
import ru.test.weather.datasource.remote.services.WeatherAPIService

class WeatherDataSourceImp(private val weatherAPIService: WeatherAPIService): WeatherDataSource {

    override suspend fun getWeather(): Result<WeatherResponse>? {
        var result: Result<WeatherResponse>? = null

        withContext(Dispatchers.IO) {
            result = try {
                val response = weatherAPIService.getWeatherInfoAsync()
                if (response.isSuccessful) {
                    Result.success(response.body())
                } else {
                    Result.error(BusinessErrorType.NETWORK_ERROR)
                }
            } catch (ex: Exception) {
                Log.e(javaClass.canonicalName, ex.toString())
                Result.error(BusinessErrorType.NETWORK_ERROR)
            }

        }

        return result
    }
}