package ru.test.weather.datasource.remote.models

import com.google.gson.annotations.SerializedName

data class WeatherResponse(
    @SerializedName("weather") val weather : List<Weather>,
    @SerializedName("main") val main : Main,
    @SerializedName("visibility") val visibility : Int,
    @SerializedName("wind") val wind : Wind,
    @SerializedName("dt") val dt : Int,
    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String
)

data class Main (
    @SerializedName("temp") val temp : Double,
    @SerializedName("pressure") val pressure : Int,
    @SerializedName("humidity") val humidity : Int,
    @SerializedName("temp_min") val temp_min : Double,
    @SerializedName("temp_max") val temp_max : Double
)

data class Weather (
    @SerializedName("id") val id : Int,
    @SerializedName("main") val main : String,
    @SerializedName("description") val description : String,
    @SerializedName("icon") val icon : String
)

data class Wind (
    @SerializedName("speed") val speed : Int,
    @SerializedName("deg") val deg : Int
)