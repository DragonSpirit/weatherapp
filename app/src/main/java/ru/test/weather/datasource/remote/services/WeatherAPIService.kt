package ru.test.weather.datasource.remote.services

import retrofit2.Response
import retrofit2.http.GET
import ru.test.weather.datasource.remote.models.WeatherResponse

interface WeatherAPIService {

    @GET("weather?id=${cityId}&appid=${apiKey}&units=${units}&lang=${lang}")
    suspend fun getWeatherInfoAsync(): Response<WeatherResponse>

    companion object {
        const val apiKey = "f2cf3a3f6a300e4a607b052e20165536"
        const val cityId = "498817"
        const val units = "metric"
        const val lang = "en"
    }
}