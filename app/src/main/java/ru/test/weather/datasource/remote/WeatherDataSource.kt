package ru.test.weather.datasource.remote

import ru.test.weather.common.dto.Result
import ru.test.weather.datasource.remote.models.WeatherResponse

interface WeatherDataSource {
    suspend fun getWeather(): Result<WeatherResponse>?
}