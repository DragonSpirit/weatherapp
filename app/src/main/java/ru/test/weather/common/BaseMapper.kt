package ru.test.weather.common

interface BaseMapper<in A, out B> {
    fun mapFrom(type: A?): B
}