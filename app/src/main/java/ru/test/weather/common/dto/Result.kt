package ru.test.weather.common.dto

import ru.test.weather.common.enums.ResultType
import ru.test.weather.common.Error

data class Result<out T>(
    var resultType: ResultType,
    val data: T? = null,
    val error: Error? = null
) {

    companion object {
        fun <T> success(data: T?): Result<T> {
            return Result(ResultType.SUCCESS, data)
        }

        fun <T> error(error: Error): Result<T> {
            return Result(ResultType.ERROR, error = error)
        }
    }
}