package ru.test.weather.common.enums

enum class ResultType {
    ERROR,
    SUCCESS,
}