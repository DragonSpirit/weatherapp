package ru.test.weather.common.constants

import ru.test.weather.common.Error

enum class BusinessErrorType: Error {
    NETWORK_ERROR,
    UNKNOWN_ERROR,
}