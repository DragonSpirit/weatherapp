package ru.test.weather.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.test.weather.domain.model.WeatherEntity
import ru.test.weather.viewmodels.WeatherViewModel
import kotlin.math.roundToInt
import android.transition.Fade
import android.transition.TransitionManager
import ru.test.weather.R


class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val weatherViewModel: WeatherViewModel by viewModel()

    private lateinit var animation: Animation

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        animation = AnimationUtils.loadAnimation(this.context, R.anim.rotate)
        reload.setOnClickListener {
            weatherViewModel.loadData()
        }
        weatherViewModel.state.observe(viewLifecycleOwner, Observer<WeatherViewModel.WeatherState> {
            when (it) {
                is WeatherViewModel.WeatherState.Success -> handleSuccess()
                is WeatherViewModel.WeatherState.Loading -> handleLoading()
                is WeatherViewModel.WeatherState.Empty -> showPlaceholder()
                is WeatherViewModel.WeatherState.Failure -> handleFailure(it.reason)
            }

        })
        weatherViewModel.weatherEntity.observe(viewLifecycleOwner, Observer {
            it?.let {
                handleSuccess()
                updateView(it)
            }
        })
    }

    fun transitionAnimation() {
        val transition = Fade()
        transition.duration = 600
        transition.addTarget(card)
        TransitionManager.beginDelayedTransition(main, transition)
    }

    fun showPlaceholder() {
        transitionAnimation()
        card.visibility = View.GONE
        placeholder.visibility = View.VISIBLE
    }

    fun hidePlaceholder() {
        transitionAnimation()
        card.visibility = View.VISIBLE
        placeholder.visibility = View.GONE
    }

    fun stopAnimation() {
        reload.clearAnimation()
    }

    fun handleLoading() {
        stopAnimation()
        reload.startAnimation(animation)
    }

    fun handleFailure(reason: String) {
        stopAnimation()
        Toast.makeText(this.context, reason, Toast.LENGTH_LONG).show()
    }

    fun handleSuccess() {
        hidePlaceholder()
        stopAnimation()
    }

    fun updateView(weatherState: WeatherEntity) {
        val addition: String = when {
            weatherState.temp > 0 -> "+"
            weatherState.temp < 0 -> "-"
            else -> ""
        }
        city.text = weatherState.name
        temperature.text = resources.getString(R.string.temperatureView, addition, weatherState.temp.roundToInt())
        wind.text = resources.getString(R.string.wind_speed, weatherState.windSpeed)
        description.text = weatherState.description
    }

}
