package ru.test.weather.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ru.test.weather.common.constants.BusinessErrorType
import ru.test.weather.domain.Failure
import ru.test.weather.domain.model.WeatherEntity
import ru.test.weather.domain.usecases.GetWeatherUseCase

class WeatherViewModel(private val getWeatherUseCase: GetWeatherUseCase) : ViewModel() {

    sealed class WeatherState {
        object Loading : WeatherState()
        object Empty : WeatherState()
        data class Success(val weatherEntity: WeatherEntity) : WeatherState()
        data class Failure(val reason: String) : WeatherState()
    }

    val state = MutableLiveData<WeatherState>()

    var weatherEntity: MutableLiveData<WeatherEntity?> = MutableLiveData(null)

    init {
        loadData()
    }

    fun loadData() {
        getWeatherUseCase.invoke(viewModelScope, Unit) { it.either(::handleFailure, ::handleSuccess) }
        state.value = WeatherState.Loading
    }

    private fun handleSuccess(weatherEntity: WeatherEntity) {
        when {
            weatherEntity.id == -1 -> state.value = WeatherState.Empty
            weatherEntity.id != -1 -> {
                this.weatherEntity.value = weatherEntity
                state.value = WeatherState.Success(weatherEntity)
            }
        }
    }

    private fun handleFailure(failure: Failure) {
        failure.exception.message?.let {
            state.value = WeatherState.Failure(it)
            return
        }

        state.value = WeatherState.Failure(BusinessErrorType.UNKNOWN_ERROR.name)
    }


}
